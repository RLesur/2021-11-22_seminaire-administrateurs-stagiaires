## La DIIT <!-- .element: class="amatic" -->

### Division Innovation et Instruction Technique <!-- .element: class="amatic" -->

![](amongus_sofa.png)

<innovation@insee.fr>

---

### Les valeurs de l'innovation

- collaboration
- ouverture
- expérimentation
- apprentissage par la pratique
- excellence technique

<!--
_une mise en application de la théorie de l'auto-détermination_

- autonomie
- compétence
- appartenance sociale

-->

---

### L'expérience innovation

- des personnes
- des services
- des technologies
<li class="fragment" data-fragment-index="2"><span>des buzzwords <i class="far fa-meh-rolling-eyes fa-lg"></i></span></li>

---

# Trop de jargon ? <!-- .element: class="amatic" -->

![](amongus_emergency.gif) <!-- .element: style="height:350px" -->

---

### Les services de l'innovation

- un soutien aux communautés de statisticiens, data scientists et développeurs
- des plateformes : le [SSPCloud](https://datalab.sspcloud.fr) et [dev.insee.io](https://dev.insee.io)
- des incubations

---

### Le SSPCloud

- plateforme orientée data science
- favorise les pratiques de science reproductible
- ouverte et collaborative
- lieu de formation et d'expérimentation
- technologies à l'état de l'art
- 100% _open source_

---

<div class="r-stretch">
<img src="macbook_onyxia.png">
</div>

<https://datalab.sspcloud.fr>

---

### Cycle de vie d'un projet data

_du libre service à la mise en production_

![](onyxia_galaxy.png)

---

### Les technologies

#### Un cycle effrené d'innovations

Les vues d'ensemble de Matt Turck  
<https://mattturck.com/data2021/>

---

### 2012

<img class="r-stretch" src="./2012_big-data-landscape.jpg">

---

### 2021

<img class="r-stretch" src="./2021-MAD-Landscape.jpg">

---

### Une conviction à partager

> Les technologies subissent des bouleversements qui finissent par transformer l'activité

- les produits changent de nature
- les méthodes de travail évoluent  
...et finiront par transformer l'organisation

---

### De statisticien à data scientist

<blockquote class="twitter-tweet" data-lang="fr"><p lang="en" dir="ltr">Data Scientist (n.): Person who is better at statistics than any software engineer and better at software engineering than any statistician.</p>&mdash; Josh Wills (@josh_wills) <a href="https://twitter.com/josh_wills/status/198093512149958656?ref_src=twsrc%5Etfw">3 mai 2012</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

---

### Data science: WTF?

_**W**hat **T**hey **F**orgot to teach you?_ <!-- .element: class="fragment" data-fragment-index="2" -->

<div class="fragment" data-fragment-index="3">

- Science reproductible
  * contrôle de version (`git`)
  * gestion des dépendances (`venv`)
  * conteneurisation (`docker`)
- Manipulation des données
  * SQL
  * bases NoSQL
- Mise en production
  * intégration continue et déploiement continu (CI/CD)
  * API (`fastapi`, `plumber`)

</div>

---

## L'innovation est-elle réellement innovante ? <!-- .element: class="amatic" -->

![](./amongus_shh.jpg)  <!-- .element: style="height: 450px" -->

---

### Le cycle de la _hype_ de Gartner

<a title="NeedCokeNow
Olga Tarkovskiy., CC BY-SA 3.0 &lt;https://creativecommons.org/licenses/by-sa/3.0&gt;, via Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Hype-Cycle-General.png"><img width="70%" alt="Hype-Cycle-General" src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/bf/Hype-Cycle-General.png/512px-Hype-Cycle-General.png"></a>

---

## Le SSPCloud : une sélection <!-- .element: class="amatic" -->

![](./amongus_choice.jpg)  <!-- .element: style="height: 450px" -->

---

## Sujets émergents <!-- .element: class="amatic" -->

<div class="r-stack">

![](./amongus_moon.jpg)  <!-- .element: class="fragment fade-out" style="height: 250px" -->

<div class="fragment">

- porter l'expérience SSPCloud à l'extérieur de l'Insee
- _data management_
- MLOps

</div>

</div>

---

# Questions <!-- .element: class="amatic" -->
